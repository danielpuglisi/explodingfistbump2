class User < ApplicationRecord
  has_secure_password

  normalizes :email, with: ->(email) { email.downcase.strip }
  validates :email, presence: true, uniqueness: true
end
