class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.authenticate_by(user_params)

    if @user
      session[:user_id] = @user.id
      redirect_to admin_dashboard_path, notice: "Logged in!"
    else
      redirect_to login_path, alert: "Email or password is invalid"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to login_path, notice: "Logged out!"
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
