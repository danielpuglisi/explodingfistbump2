require "test_helper"

class UserTest < ActiveSupport::TestCase
  def user
    @user ||= users(:mr_robot)
  end

  test "#valid?" do
    user = User.new
    user.valid?
    assert user.errors.added?(:email, :blank)

    user.email = 'elliot.anderson@ecorp.com'
    user.valid?
    assert user.errors.added?(:email, :taken, value: 'elliot.anderson@ecorp.com')
  end

  test "should normalize emails" do
    user = User.new
    user.email = ' Max.Muster@gmail.com '
    user.valid?
    assert_equal 'max.muster@gmail.com', user.email
  end
end
