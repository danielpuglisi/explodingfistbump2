require 'test_helper'

class RegistrationsControllerTest < ActionController::TestCase
  def user
    @user ||= users(:mr_robot)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create a user' do
    assert_difference("User.count") do
      post :create, params: { user: { email: 'max.muster@gmail.com', password: 'supersecret', password_confirmation: 'supersecret' } }
    end
    assert_redirected_to admin_dashboard_path
  end
end
