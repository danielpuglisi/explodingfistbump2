require 'test_helper'

class Admin::DashboardControllerTest < ActionController::TestCase
  def user
    @user ||= users(:mr_robot)
  end

  test "should get index" do
    sign_in(user)
    get :index
    assert_response :success
  end

  test "should not get index" do
    get :index
    assert_redirected_to login_path
  end
end
