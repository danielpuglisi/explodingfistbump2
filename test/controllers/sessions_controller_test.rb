require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  def user
    @user ||= users(:mr_robot)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create session' do
    post :create, params: { user: { email: 'elliot.anderson@ecorp.com', password: 'supersecret' } }
    assert_redirected_to admin_dashboard_path
  end

  test 'should not create session' do
    post :create, params: { user: { email: 'elliot.anderson@ecorp.com', password: 'wrong' } }
    assert_redirected_to login_path
  end

  test 'should destroy session' do
    sign_in(user)
    delete :destroy
    assert_redirected_to login_path
  end
end
